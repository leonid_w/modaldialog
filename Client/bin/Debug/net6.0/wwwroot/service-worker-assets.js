﻿self.assetsManifest = {
  "assets": [
    {
      "hash": "sha256-9oL5L0vy4diFowTj7WZsYafcT956yS5mSrJCHHV7w0Y=",
      "url": "_framework\/blazor.boot.json"
    },
    {
      "hash": "sha256-2a4+w+GPD0ba\/uDmYNwHC6NOWNgWyURfhXiEcrQRwwU=",
      "url": "_framework\/Microsoft.AspNetCore.Authorization.dll"
    },
    {
      "hash": "sha256-eA2UQKfF2QfKq1RFvC9BU3BazGkmek0\/9vbyzDOXGvc=",
      "url": "_framework\/Microsoft.AspNetCore.Components.dll"
    },
    {
      "hash": "sha256-51EAcz5hqAaKgW2NzWin03wMI\/j4XfsSdP0uwd7AHNE=",
      "url": "_framework\/Microsoft.AspNetCore.Components.Forms.dll"
    },
    {
      "hash": "sha256-pISYxWkH4ZfilZrt1Bz2m4ffQSpV9CMF3QOerji3ge0=",
      "url": "_framework\/Microsoft.AspNetCore.Components.Web.dll"
    },
    {
      "hash": "sha256-2h\/IP0MjLY7RFch1DM7\/oA0TQ7G9umPnxI9rk8CW1Wg=",
      "url": "_framework\/Microsoft.AspNetCore.Components.WebAssembly.dll"
    },
    {
      "hash": "sha256-wpYCKtAL4H03XTzfMe8OOEty2tuihVWZ21\/qCSbyf+4=",
      "url": "_framework\/Microsoft.AspNetCore.Metadata.dll"
    },
    {
      "hash": "sha256-kMQv+g+Y4epeIl\/KKHiMxQuSZc4QoGlikMV\/ujzjIGk=",
      "url": "_framework\/Microsoft.Extensions.Configuration.dll"
    },
    {
      "hash": "sha256-I9grxhS0dPyQuXUslzbD1+bcjFndE2r329OktmcL5YU=",
      "url": "_framework\/Microsoft.Extensions.Configuration.Abstractions.dll"
    },
    {
      "hash": "sha256-+bCuEuLiDRhL3C6Hyt2qy+MTAX8+kygcxFrJncOLghU=",
      "url": "_framework\/Microsoft.Extensions.Configuration.Binder.dll"
    },
    {
      "hash": "sha256-u+\/MTA6SA7kIsDN9ZJpe3oQtYQLACR\/3EvfurVwCHWU=",
      "url": "_framework\/Microsoft.Extensions.Configuration.FileExtensions.dll"
    },
    {
      "hash": "sha256-lYGAhGbLICRiY1qO+\/r1ZHvewDZqkhmEw6Gvjk0+nRk=",
      "url": "_framework\/Microsoft.Extensions.Configuration.Json.dll"
    },
    {
      "hash": "sha256-jMAPcvT9Lm3h3EZXxyIWpS7KpUEHOWqNIyymxBesxmc=",
      "url": "_framework\/Microsoft.Extensions.DependencyInjection.dll"
    },
    {
      "hash": "sha256-IJTIJ3b3UjBB4o6yT1+fjCexP5HfEN4JTAha61oYDwA=",
      "url": "_framework\/Microsoft.Extensions.DependencyInjection.Abstractions.dll"
    },
    {
      "hash": "sha256-ULiBHYBcdPX+pQeVGrPP4XpZE+nt6HCSqIHhWjUdkk0=",
      "url": "_framework\/Microsoft.Extensions.FileProviders.Abstractions.dll"
    },
    {
      "hash": "sha256-RCfi6eobFBLMA4I5P4VuQgu+L6b1Go5gNV63iRIf9HQ=",
      "url": "_framework\/Microsoft.Extensions.FileProviders.Physical.dll"
    },
    {
      "hash": "sha256-vrnsOx9yovhWJgR7Ynz6oJdAunZlJOUv5mEEjc1UbWk=",
      "url": "_framework\/Microsoft.Extensions.FileSystemGlobbing.dll"
    },
    {
      "hash": "sha256-I5oa1VAvWMSiroy+aj5FGnaTR\/nn3j1U4FHiA\/H+xQo=",
      "url": "_framework\/Microsoft.Extensions.Logging.dll"
    },
    {
      "hash": "sha256-GwN015XIUC3TjQ7XnFuev2skK+\/Va1dRwnrxbdfyJzk=",
      "url": "_framework\/Microsoft.Extensions.Logging.Abstractions.dll"
    },
    {
      "hash": "sha256-XmAqNh5mZIaFlbrC\/xydETkPwOiK+fqglvxuhbcm3l0=",
      "url": "_framework\/Microsoft.Extensions.Options.dll"
    },
    {
      "hash": "sha256-RPakZOTicxg+VB0SYRTbQlcBSz4ujl7\/J0CgNNhQD\/Y=",
      "url": "_framework\/Microsoft.Extensions.Primitives.dll"
    },
    {
      "hash": "sha256-JfrUt3OV8MA3BuApeljKRJrPkaUmXM2JNYdhpWbF7v0=",
      "url": "_framework\/Microsoft.JSInterop.dll"
    },
    {
      "hash": "sha256-OoV6iMxrwVu3O8m7lTV4EtpgIRGuwD9b3MpvLBml6XE=",
      "url": "_framework\/Microsoft.JSInterop.WebAssembly.dll"
    },
    {
      "hash": "sha256-LzGTDXCdaGz8Jukwu1g2boF1UsnEC9jtFUmkU0RZjeM=",
      "url": "_framework\/System.IO.Pipelines.dll"
    },
    {
      "hash": "sha256-+u77Zl0XA1c3bwof3lzaHjB2upyetIxtzkfUk7cXkPc=",
      "url": "_framework\/Microsoft.CSharp.dll"
    },
    {
      "hash": "sha256-CVhzokZlxINChKE0x+RmN2n2iVRQP\/rEWuDsVz6l\/GQ=",
      "url": "_framework\/Microsoft.VisualBasic.Core.dll"
    },
    {
      "hash": "sha256-G2XxRhPD1EjGzPB54yAUVfWmXQgxU3SqNyidq4s2sms=",
      "url": "_framework\/Microsoft.VisualBasic.dll"
    },
    {
      "hash": "sha256-ilkNbltoBJa3X3Yvdeg5vLGnkKb1R7TNtTPZa97dWlg=",
      "url": "_framework\/Microsoft.Win32.Primitives.dll"
    },
    {
      "hash": "sha256-WO8Omv3anUSw2h7Z1CtN0vXNnaic6hYYaPQrGKfEGso=",
      "url": "_framework\/Microsoft.Win32.Registry.dll"
    },
    {
      "hash": "sha256-6xW+cYvsDKggpKuw125BaP740Kwqdees0YZbgxY5N+U=",
      "url": "_framework\/System.AppContext.dll"
    },
    {
      "hash": "sha256-31CaCIk0OyBNzUE\/Q0ztOPi+NyYmwwRxtEpsE3zlAdI=",
      "url": "_framework\/System.Buffers.dll"
    },
    {
      "hash": "sha256-RsLvA5UDeUNtUSuTkQE99y3kBYXwpR3EfH4CR+d7unc=",
      "url": "_framework\/System.Collections.Concurrent.dll"
    },
    {
      "hash": "sha256-CAqxr2UloELte2eFN\/k8cs6p4z7QVv3C1vRsj0wLm0w=",
      "url": "_framework\/System.Collections.Immutable.dll"
    },
    {
      "hash": "sha256-YafEoTmqInx3rfab9miPqDQ55Vvi8xwf2M7wrQXOZkI=",
      "url": "_framework\/System.Collections.NonGeneric.dll"
    },
    {
      "hash": "sha256-dSztKWne5gfDJjUJr1LBzhEVRq8ael2NeLHRkcmMYAA=",
      "url": "_framework\/System.Collections.Specialized.dll"
    },
    {
      "hash": "sha256-6gCG2wYjtKiO10s40D8s3Y5AReeK1K2V4LWQwdjA6TE=",
      "url": "_framework\/System.Collections.dll"
    },
    {
      "hash": "sha256-8TJOEOAh8Drsspfudn441oOp20iyYhL5BuGG1JIktCU=",
      "url": "_framework\/System.ComponentModel.Annotations.dll"
    },
    {
      "hash": "sha256-2kw\/mrCKsEAh95UhhfUnSB6bH\/zn65+iz4PtWmqB65w=",
      "url": "_framework\/System.ComponentModel.DataAnnotations.dll"
    },
    {
      "hash": "sha256-nmvhQcoyeRzBZ8GcyPcuI4RkfEB4p1ijiKjMpZVwHc0=",
      "url": "_framework\/System.ComponentModel.EventBasedAsync.dll"
    },
    {
      "hash": "sha256-GTslZ5NW6YGeMNIzmhbz3Z0pq9tuzsel\/aFtWs5EVUs=",
      "url": "_framework\/System.ComponentModel.Primitives.dll"
    },
    {
      "hash": "sha256-\/ZJXHqqdzgQN8HeP+7rm\/6JlLCMY\/Xw4yhBX6bYpui4=",
      "url": "_framework\/System.ComponentModel.TypeConverter.dll"
    },
    {
      "hash": "sha256-4Yc3eKrrjyf4lMpDeZENKI+dQYdFWHXXvmkPBW8WZEY=",
      "url": "_framework\/System.ComponentModel.dll"
    },
    {
      "hash": "sha256-F9a2JztQJ5D\/aWi5FH0y\/H2w2vDk\/2++HQqAkYFR+f8=",
      "url": "_framework\/System.Configuration.dll"
    },
    {
      "hash": "sha256-ems3V1qPUl8BJBJVeFeRFTqqWgCflTM89ARtjjan4FA=",
      "url": "_framework\/System.Console.dll"
    },
    {
      "hash": "sha256-9j3zAe8PSOAHu7LXDmTYbxMbxMwNxr4sO5GGuU+6SUk=",
      "url": "_framework\/System.Core.dll"
    },
    {
      "hash": "sha256-vNdFQV\/A\/NnMVSOLqFwTV+5tbm1HDDAz1U8foPxuDqk=",
      "url": "_framework\/System.Data.Common.dll"
    },
    {
      "hash": "sha256-0rovm25Oa076IGYvKdxdWQOaQuDynYA0fm1xM33eEro=",
      "url": "_framework\/System.Data.DataSetExtensions.dll"
    },
    {
      "hash": "sha256-wjtlPQIDG69EdXjwc8fnqKlU0rFgP\/2eKCeDiyEH\/00=",
      "url": "_framework\/System.Data.dll"
    },
    {
      "hash": "sha256-2sxTyKW9lMzV9Ca2XcZDpQHFSN3CfEKBfgOcrwM6aOM=",
      "url": "_framework\/System.Diagnostics.Contracts.dll"
    },
    {
      "hash": "sha256-PRJ+d0F\/3R1iK7cSjW\/c\/eMGeSEAi5vRRnMs20Y764U=",
      "url": "_framework\/System.Diagnostics.Debug.dll"
    },
    {
      "hash": "sha256-+LZyBS6Ma5ZtY4LwCwdelG3\/XaFQeC546pKVz+Ww4fM=",
      "url": "_framework\/System.Diagnostics.DiagnosticSource.dll"
    },
    {
      "hash": "sha256-NyvmMy9LOtmo9Mv+8ex0nIiayO3QbS+nm+TDOND1ZpA=",
      "url": "_framework\/System.Diagnostics.FileVersionInfo.dll"
    },
    {
      "hash": "sha256-Z1KW2VMmIpYv4dyzVFXmKB01fkjv6t7eV\/MQqEhaXcs=",
      "url": "_framework\/System.Diagnostics.Process.dll"
    },
    {
      "hash": "sha256-yqQsoWzfHf1Adk0E6zagibcBE4WoQRc1\/aK1eGMuC6w=",
      "url": "_framework\/System.Diagnostics.StackTrace.dll"
    },
    {
      "hash": "sha256-2czREY\/GKUUXweqTRSNR2esDq0WWNcZmeEgPV7VNTUc=",
      "url": "_framework\/System.Diagnostics.TextWriterTraceListener.dll"
    },
    {
      "hash": "sha256-WtT7xdUvRrI87vecwSz26Ki4pMCtexVURBLb+H29ZhY=",
      "url": "_framework\/System.Diagnostics.Tools.dll"
    },
    {
      "hash": "sha256-0ePTs6rcdXhrudzJUqYIy6ZgDFqMuER+YMT9ARRecJU=",
      "url": "_framework\/System.Diagnostics.TraceSource.dll"
    },
    {
      "hash": "sha256-EWa1LbY2DfM1++647C67nV8lhX45X+uNP6QvCaEm4CU=",
      "url": "_framework\/System.Diagnostics.Tracing.dll"
    },
    {
      "hash": "sha256-L5ItOSuLd0fIQh3wEnr1V339Jjy\/S35y+\/8nIIwv5nc=",
      "url": "_framework\/System.Drawing.Primitives.dll"
    },
    {
      "hash": "sha256-R+UtlKl7nG+fvrcUwaZFd2XWFMA10RA\/wzdzEiP2BAk=",
      "url": "_framework\/System.Drawing.dll"
    },
    {
      "hash": "sha256-o1ikFZ7HDplO+cnbLVJg2Efd4UmgQOOcaX4A1erKG5I=",
      "url": "_framework\/System.Dynamic.Runtime.dll"
    },
    {
      "hash": "sha256-1GQyYXs\/cTgjSvClNOlU646jZ6doUPEx3ZBDRGiMSrs=",
      "url": "_framework\/System.Formats.Asn1.dll"
    },
    {
      "hash": "sha256-FyzAJuq0xYGdTQYr9VZ2hzdLOu9zFUfujX4onZpdLHo=",
      "url": "_framework\/System.Globalization.Calendars.dll"
    },
    {
      "hash": "sha256-X4+GUhjnGDJ29Lgj17Arv2Sl9CHyyqYOkatqpoNxmV0=",
      "url": "_framework\/System.Globalization.Extensions.dll"
    },
    {
      "hash": "sha256-D+DCOVHZ+It9gxcNJzNAtYOGgiovxKND8UExPFAb+P0=",
      "url": "_framework\/System.Globalization.dll"
    },
    {
      "hash": "sha256-yGgzQ9ePHC\/8FBErxZ7q28geWW9IKvV9tZAnHgpPiMs=",
      "url": "_framework\/System.IO.Compression.Brotli.dll"
    },
    {
      "hash": "sha256-LqejF0F9e9XW9EkVTltdyIhiGwzQGeARTWEeTFN9hOA=",
      "url": "_framework\/System.IO.Compression.FileSystem.dll"
    },
    {
      "hash": "sha256-axyhqDVUAAqRqmfBSBpso3kTcgZ6CywKifLlQU4UhOk=",
      "url": "_framework\/System.IO.Compression.ZipFile.dll"
    },
    {
      "hash": "sha256-RwfbCpfC4TX2TFjBB3KaTjhNuX+PtgWyjCE8+kEUYZI=",
      "url": "_framework\/System.IO.Compression.dll"
    },
    {
      "hash": "sha256-UYIbGJm9s+t+jX0FqxQC8SxtbpIu0MVZROw4A3cIEDg=",
      "url": "_framework\/System.IO.FileSystem.AccessControl.dll"
    },
    {
      "hash": "sha256-gG4KQCc9FBmmy4lEwsedQNaT\/C3ngl8KK3RvFeEY1uA=",
      "url": "_framework\/System.IO.FileSystem.DriveInfo.dll"
    },
    {
      "hash": "sha256-x2loY\/ZhnNxM5NfhIjDUjSqoSSA05rpv5Nhh9+2h0h8=",
      "url": "_framework\/System.IO.FileSystem.Primitives.dll"
    },
    {
      "hash": "sha256-2BfK+2v3PERr8kULws\/efzEK59gzBboda8PvJBTxkmM=",
      "url": "_framework\/System.IO.FileSystem.Watcher.dll"
    },
    {
      "hash": "sha256-A9MShM+VNAEPKI0EsfpQo8ZCGQ+GqU5y4o6PzWVHmz0=",
      "url": "_framework\/System.IO.FileSystem.dll"
    },
    {
      "hash": "sha256-iVnrzdw3gihFNLnQBy1WsZrB2rp85U0FZq1ueBYNQ6A=",
      "url": "_framework\/System.IO.IsolatedStorage.dll"
    },
    {
      "hash": "sha256-oU\/GuSaRCyb\/i\/+ZbLeEtMutwlvXcDiu3rnzpxti80k=",
      "url": "_framework\/System.IO.MemoryMappedFiles.dll"
    },
    {
      "hash": "sha256-lGrBRJ3LJLNn9K2nHbP0L3rLjAPExN0b0Q93MPix6wc=",
      "url": "_framework\/System.IO.Pipes.AccessControl.dll"
    },
    {
      "hash": "sha256-omxukT4HGA\/T8TPE00kem2CBBXCcB87JTKdRe8vALho=",
      "url": "_framework\/System.IO.Pipes.dll"
    },
    {
      "hash": "sha256-SIwaaGd+lMq1X0zh0WVqzoshf+tbju7OluCyMZ8u5w8=",
      "url": "_framework\/System.IO.UnmanagedMemoryStream.dll"
    },
    {
      "hash": "sha256-BJp9BGI0xaJuM435ezIvBlTbWsvbHTFpzcUwsTI5KcQ=",
      "url": "_framework\/System.IO.dll"
    },
    {
      "hash": "sha256-liYnge3s8YHzt7\/YBmtlC0o5gcMRHLnQ6cykmmFlQ00=",
      "url": "_framework\/System.Linq.Expressions.dll"
    },
    {
      "hash": "sha256-2bjRYDph2AxxizwoVvRcCi0eB+bzIvmfqiMoZ8HvPeU=",
      "url": "_framework\/System.Linq.Parallel.dll"
    },
    {
      "hash": "sha256-v3JFTfXh2EH1lKYXaJU6j7JBIjgOGUiY+wSjyDkLueA=",
      "url": "_framework\/System.Linq.Queryable.dll"
    },
    {
      "hash": "sha256-tWCXxu4vmSn6\/mGC7Syam87E+JX8K3tbTkmXtnf\/D0Q=",
      "url": "_framework\/System.Linq.dll"
    },
    {
      "hash": "sha256-JTy1zDyulsLVx0OrqaFL\/N8DsBtRxDkagbpC4x8PfiQ=",
      "url": "_framework\/System.Memory.dll"
    },
    {
      "hash": "sha256-ZrZpXyBRbWweAEbwrxWvU4wl0E1fJVmRZYIrjkfiv9k=",
      "url": "_framework\/System.Net.Http.Json.dll"
    },
    {
      "hash": "sha256-KBYU2sySq+ltp0W0rkrGa1er8AF0LkDtUOfvDEIaRNc=",
      "url": "_framework\/System.Net.Http.dll"
    },
    {
      "hash": "sha256-yqMqiuZ8NkUyVxVgbC9zvauOdrLKFn+\/A4Zd36UzVW4=",
      "url": "_framework\/System.Net.HttpListener.dll"
    },
    {
      "hash": "sha256-mSv\/p5Uyy9SgyJkDNE3jfDrPaFMb8gVS3Pd+xglfnfE=",
      "url": "_framework\/System.Net.Mail.dll"
    },
    {
      "hash": "sha256-tbfcoVV6OUltVrcDV7hQ3xvnM6sN8mfVUgChNfWIIOI=",
      "url": "_framework\/System.Net.NameResolution.dll"
    },
    {
      "hash": "sha256-gk0\/\/fn5bA88KZXcZkuxqSDv0sjjnJS6hOFzqbouaWU=",
      "url": "_framework\/System.Net.NetworkInformation.dll"
    },
    {
      "hash": "sha256-AyaMZv5pwU6DbzvrHTmCJxgEQsvklK8K8+Brg9NSh0o=",
      "url": "_framework\/System.Net.Ping.dll"
    },
    {
      "hash": "sha256-yk7lx4hHdTAA8Yj36ERYlI4vv8TyhVktW9Ylv+psoFE=",
      "url": "_framework\/System.Net.Primitives.dll"
    },
    {
      "hash": "sha256-VnjpO2+5Hc9OwcSBIkeNlBCnIHtsqOQxQQTf+5UvTfc=",
      "url": "_framework\/System.Net.Quic.dll"
    },
    {
      "hash": "sha256-bRm9qpV+jwOphiEi7pPZnRhRXLXZ1neytcjdg044F3k=",
      "url": "_framework\/System.Net.Requests.dll"
    },
    {
      "hash": "sha256-fZuXKYDxgeUinnxMaPtGaFzyC+kiCZq9MTrUBxwCRlY=",
      "url": "_framework\/System.Net.Security.dll"
    },
    {
      "hash": "sha256-JMntIbK+uS0RPH+1AxLGZ08kxrkTPUp0ds9cNUN0LMk=",
      "url": "_framework\/System.Net.ServicePoint.dll"
    },
    {
      "hash": "sha256-mTg7dKpidnOfgOLsy5CKjdlFfgQNnIStESMUsocGZXY=",
      "url": "_framework\/System.Net.Sockets.dll"
    },
    {
      "hash": "sha256-y6u23Op\/6f3w9tNIIUfcMnBgP+6UmMLFB3lcbYr8tx8=",
      "url": "_framework\/System.Net.WebClient.dll"
    },
    {
      "hash": "sha256-V+i8cEMzjaPYciGF7+uxquBw4OR6xWE2B7Q5ivMkENM=",
      "url": "_framework\/System.Net.WebHeaderCollection.dll"
    },
    {
      "hash": "sha256-peApA+Ii5IzeWPR5lOIWtkx06PvF3mHVAt9xfgAX+dI=",
      "url": "_framework\/System.Net.WebProxy.dll"
    },
    {
      "hash": "sha256-GquBiCtdNo325wCQETaIw1H8a17MW8SRYKAhYGUeSu0=",
      "url": "_framework\/System.Net.WebSockets.Client.dll"
    },
    {
      "hash": "sha256-6078RoKS5gViTMqExIW1Xs4P4Y4wm57VtlBitF8WPwk=",
      "url": "_framework\/System.Net.WebSockets.dll"
    },
    {
      "hash": "sha256-E0CV2RMEQRs9a+WVuZOE\/STHlmuho0j2ZOPUOXdUsHg=",
      "url": "_framework\/System.Net.dll"
    },
    {
      "hash": "sha256-QWmXkr4c9Xn+5dV4Qu3hEMhyTHDcZD5OlNKUGX6I7Y4=",
      "url": "_framework\/System.Numerics.Vectors.dll"
    },
    {
      "hash": "sha256-ub6HoHUSqw0RDft6gaO2wJSIcN8o4Q7iAxuWMSg7O9w=",
      "url": "_framework\/System.Numerics.dll"
    },
    {
      "hash": "sha256-7yNeT4m1+zY\/eQcE\/iWABqlBeefHWCrzM0u5Ij2Wfp8=",
      "url": "_framework\/System.ObjectModel.dll"
    },
    {
      "hash": "sha256-ssQbSZHwz\/2nyS2wGXb6xsJJgvW\/CS7CxgPxgJbAKsU=",
      "url": "_framework\/System.Private.DataContractSerialization.dll"
    },
    {
      "hash": "sha256-sfOYdQIFGtYYlxn9\/FIFu28+AmRiKB2avQ+o0qqN9Bg=",
      "url": "_framework\/System.Private.Runtime.InteropServices.JavaScript.dll"
    },
    {
      "hash": "sha256-NPfAnfx3wolSikRVz7z42TMA967Yim0j\/gA8gHTFTFI=",
      "url": "_framework\/System.Private.Uri.dll"
    },
    {
      "hash": "sha256-Gpk+ELtIm0v4vTiLgeLIq7BO0NzqQZwnyItIjFV\/MXw=",
      "url": "_framework\/System.Private.Xml.Linq.dll"
    },
    {
      "hash": "sha256-APYI8uQ121C16PWLINx6P\/6DwW6RAIeBNXJ2rNDM2q0=",
      "url": "_framework\/System.Private.Xml.dll"
    },
    {
      "hash": "sha256-4IqeBmCZRhT4OHH27xBOVlBURpXbifcxeGVzTMMXKQs=",
      "url": "_framework\/System.Reflection.DispatchProxy.dll"
    },
    {
      "hash": "sha256-74ZHpg\/HMg60mX6GrQKmE4\/amwm0DoKFL3pQwOplMwA=",
      "url": "_framework\/System.Reflection.Emit.ILGeneration.dll"
    },
    {
      "hash": "sha256-dPVtqGHZ8JDIHqQM3kC2YO7f7BO+CfJjIvVTtVI6018=",
      "url": "_framework\/System.Reflection.Emit.Lightweight.dll"
    },
    {
      "hash": "sha256-4hvJKk5rCb3ASimJ2bV9M5fNMju1mPeIxoHvOr9OKdk=",
      "url": "_framework\/System.Reflection.Emit.dll"
    },
    {
      "hash": "sha256-Ii1FgfAvKWcZV7Dr1lfqli1iGLCMbp7GLgmQlYmDSg8=",
      "url": "_framework\/System.Reflection.Extensions.dll"
    },
    {
      "hash": "sha256-TlOtB5kxUXXTPn18i4isgFzu7C7lryqH\/w6+ch9tEuE=",
      "url": "_framework\/System.Reflection.Metadata.dll"
    },
    {
      "hash": "sha256-DODdHhJmASosTDiAQ1hpkzs3dn68w9ol3rzeuNjOuwo=",
      "url": "_framework\/System.Reflection.Primitives.dll"
    },
    {
      "hash": "sha256-BZT4xRoTpKSlimsHDaLXe4dCZdunI7XTdom1Lrg3pgg=",
      "url": "_framework\/System.Reflection.TypeExtensions.dll"
    },
    {
      "hash": "sha256-TxnmKZ0AoLLO08VCuqbTZTPiO3SW7QSwcNl4f2RGFO8=",
      "url": "_framework\/System.Reflection.dll"
    },
    {
      "hash": "sha256-aBNtfMAn0C\/VYE+6aMnPpIwajXMXiHolHI0BmMnWHCM=",
      "url": "_framework\/System.Resources.Reader.dll"
    },
    {
      "hash": "sha256-Ug6KD6hLMJ1UgfHlO1LMsYj20Dq6qAtcl6nmrhTbc\/4=",
      "url": "_framework\/System.Resources.ResourceManager.dll"
    },
    {
      "hash": "sha256-8WuADKzk3bK4ilDMoiVc+Ix5y6WbyTMAn0hfcAyAPkI=",
      "url": "_framework\/System.Resources.Writer.dll"
    },
    {
      "hash": "sha256-3h1jEqObClhv7mO29PqL\/g2nNZjw3QI3\/c\/RMx0YEmM=",
      "url": "_framework\/System.Runtime.CompilerServices.Unsafe.dll"
    },
    {
      "hash": "sha256-Ym1tRCrv1MUvUMEY5qGlpd94Cz7W0\/qVnM\/FH9ZPapk=",
      "url": "_framework\/System.Runtime.CompilerServices.VisualC.dll"
    },
    {
      "hash": "sha256-tvG1P5rrjBYEuRTKntu4mwu1t02TxGHTZbmzN+271V0=",
      "url": "_framework\/System.Runtime.Extensions.dll"
    },
    {
      "hash": "sha256-CDw\/ZHV2CbyqZ5q7\/ccEGbsap0xgha8IWh8nN5AyLp0=",
      "url": "_framework\/System.Runtime.Handles.dll"
    },
    {
      "hash": "sha256-hcSgKL0p7ET5otSBxQybCNo1HDEF\/7FHkJzdxm1Z94k=",
      "url": "_framework\/System.Runtime.InteropServices.RuntimeInformation.dll"
    },
    {
      "hash": "sha256-YWeUkY3h4FjdBQxQJIXsE2ri5yx02kPEniSvLo4B0wc=",
      "url": "_framework\/System.Runtime.InteropServices.dll"
    },
    {
      "hash": "sha256-dJBjWuVLpvvuy\/UmxohlnHeuyCZX7s9pxkvOLpkqfRo=",
      "url": "_framework\/System.Runtime.Intrinsics.dll"
    },
    {
      "hash": "sha256-DkuGfpt7tQ3OQ4c22YLUHg0fpRDwybbw9Xs7W8o66Pc=",
      "url": "_framework\/System.Runtime.Loader.dll"
    },
    {
      "hash": "sha256-9TeuAhOAfb9A5MI8AFrwAcx3dg9DEU3bjZWBF13bSqc=",
      "url": "_framework\/System.Runtime.Numerics.dll"
    },
    {
      "hash": "sha256-DgVfZ4iMwfURjLdPaVFVcIyr2OqkqRtQAj162xFmKog=",
      "url": "_framework\/System.Runtime.Serialization.Formatters.dll"
    },
    {
      "hash": "sha256-T2tv7lewgdx2MKOPYIssOlqL+9Umb3nIkMjzAa9Ym2s=",
      "url": "_framework\/System.Runtime.Serialization.Json.dll"
    },
    {
      "hash": "sha256-y9px7d7BQOQ4zwfalDAP22XOCW4BastG1Qe+S5GPcMo=",
      "url": "_framework\/System.Runtime.Serialization.Primitives.dll"
    },
    {
      "hash": "sha256-xw9wj47CJlcMBWN+iMiHWyL5VOYpBDjDEZ\/LVfRSuIo=",
      "url": "_framework\/System.Runtime.Serialization.Xml.dll"
    },
    {
      "hash": "sha256-IpwxMxNZ7rUqAumN9yGRG7NHIrktw5LMxHa4SvDmy4g=",
      "url": "_framework\/System.Runtime.Serialization.dll"
    },
    {
      "hash": "sha256-zuL3D7kIOFRyH437PfpDF\/GurMLGzLs58MC0d2jsNjI=",
      "url": "_framework\/System.Runtime.dll"
    },
    {
      "hash": "sha256-5UWP0Ogtfn4atK00oLrvnGsLh0DE+7l9wwL\/kKgRHz4=",
      "url": "_framework\/System.Security.AccessControl.dll"
    },
    {
      "hash": "sha256-m4Wn0oA1TfrGTWxVKfZ4XGI4pWCDAojvGOC4oDOcYeM=",
      "url": "_framework\/System.Security.Claims.dll"
    },
    {
      "hash": "sha256-o1hbqwH04vPyaEWWP+h\/GUsFF2kpm3QkUZjhcpoYOFQ=",
      "url": "_framework\/System.Security.Cryptography.Algorithms.dll"
    },
    {
      "hash": "sha256-nvihXRwq1p3NXT\/TUL+rouGdABlSvjI5Lrea7W1KYxc=",
      "url": "_framework\/System.Security.Cryptography.Cng.dll"
    },
    {
      "hash": "sha256-0tIqo5p4d8bhMeKN5YjUfFXsZ5W8TaXGR4mRaV44LpY=",
      "url": "_framework\/System.Security.Cryptography.Csp.dll"
    },
    {
      "hash": "sha256-9GuWTz3tIern5YyS6+WNbS6JtoV8hTK9cRWtQnHSrMc=",
      "url": "_framework\/System.Security.Cryptography.Encoding.dll"
    },
    {
      "hash": "sha256-w4iXIbRmdB3JLtsMzMFEKyC7oNq507WxdLEdDnpzmLw=",
      "url": "_framework\/System.Security.Cryptography.OpenSsl.dll"
    },
    {
      "hash": "sha256-4m0zjC1864rOVz6VV7KZVdKLHiZothOi9ZcqeM7KJXI=",
      "url": "_framework\/System.Security.Cryptography.Primitives.dll"
    },
    {
      "hash": "sha256-VqckwtkujDu9\/snsCSoZ4Yc9C\/DMEsIBTly3vmQwIpY=",
      "url": "_framework\/System.Security.Cryptography.X509Certificates.dll"
    },
    {
      "hash": "sha256-q5jalfJyJvMUMhbQ+WhUbs5E7AdCz\/MfTcDBuVgtbAg=",
      "url": "_framework\/System.Security.Principal.Windows.dll"
    },
    {
      "hash": "sha256-WkISvV3nv9Ud3d0Nit1\/931PhIJoRRt6iFncrvFPlmE=",
      "url": "_framework\/System.Security.Principal.dll"
    },
    {
      "hash": "sha256-0t9Tk3K0VcQneKtyOcbBckBEGBP7px1pfM0xNLIM2uY=",
      "url": "_framework\/System.Security.SecureString.dll"
    },
    {
      "hash": "sha256-E2eZIM1EvUYIGABAFBCLFGZIut6Vc3BPkqRsSoXMbHw=",
      "url": "_framework\/System.Security.dll"
    },
    {
      "hash": "sha256-pIJWsZjOH+FuupDPnj9FLsGijj8hNBYGG4jUjGdJEXE=",
      "url": "_framework\/System.ServiceModel.Web.dll"
    },
    {
      "hash": "sha256-AtEnixFAMmI1cfjarFJ8ci03hSK4kMNg3XRmviBDg94=",
      "url": "_framework\/System.ServiceProcess.dll"
    },
    {
      "hash": "sha256-jVJ4XazBA1gMRybvRwkiCfeNWrq6oxTlGeAQsmdNwsg=",
      "url": "_framework\/System.Text.Encoding.CodePages.dll"
    },
    {
      "hash": "sha256-poOXmbxInYAcIg9DOmFeFnbdIDO7qkwpApeWdzvlOZQ=",
      "url": "_framework\/System.Text.Encoding.Extensions.dll"
    },
    {
      "hash": "sha256-\/z7kc60unY0KvOMi0FhshuOIKxPx3cLIccS92RIUzEk=",
      "url": "_framework\/System.Text.Encoding.dll"
    },
    {
      "hash": "sha256-1YU\/ufH3iy8ze2CCd3CO\/5W4VfFnQBlcRYgSEQqxncM=",
      "url": "_framework\/System.Text.Encodings.Web.dll"
    },
    {
      "hash": "sha256-UvrRUPhg13SO5ExjHZQH96I8z4V+CrAcQNpmFGFUkvI=",
      "url": "_framework\/System.Text.Json.dll"
    },
    {
      "hash": "sha256-uL443gBwLUAQLOjvhXD8mf4q7kPX2SdBqOU\/2oJO1ec=",
      "url": "_framework\/System.Text.RegularExpressions.dll"
    },
    {
      "hash": "sha256-ohLxnP1eocn7q7EAPiqtWtRxkCmxcLqYPeFVlWq6Fik=",
      "url": "_framework\/System.Threading.Channels.dll"
    },
    {
      "hash": "sha256-0EIY4cHci0vnyFmBFJF2dlr4btJuOTgiJZklsFAmdo4=",
      "url": "_framework\/System.Threading.Overlapped.dll"
    },
    {
      "hash": "sha256-fXYQxma0L+FE3JY8mg+QZQQn4IUAX9m3ou7\/3kgFzhQ=",
      "url": "_framework\/System.Threading.Tasks.Dataflow.dll"
    },
    {
      "hash": "sha256-OpHvo5lK4SOI\/zbpXIQaqHOBAiLF7fY+xfaBwrYlBXk=",
      "url": "_framework\/System.Threading.Tasks.Extensions.dll"
    },
    {
      "hash": "sha256-rpD2XP9SutarbSgNNDxv76FrFwtH27NvivJPjMQXJsk=",
      "url": "_framework\/System.Threading.Tasks.Parallel.dll"
    },
    {
      "hash": "sha256-nZYS+58rEiBUe+zAoWxAwiX0BX1CtpCi8rx1O4bXMas=",
      "url": "_framework\/System.Threading.Tasks.dll"
    },
    {
      "hash": "sha256-5q+s8JsmimslIJU1uKv4Lqu0VX5S3aUuMyxqZb4rYsk=",
      "url": "_framework\/System.Threading.Thread.dll"
    },
    {
      "hash": "sha256-m5zV0QVBdezeYPJpCEmuPSnQOK7TFFRjUy7UCNquh6Q=",
      "url": "_framework\/System.Threading.ThreadPool.dll"
    },
    {
      "hash": "sha256-yHgwGYRon\/M5G6xsqfO9WX6kc7NdjItU44PIyWtEHcA=",
      "url": "_framework\/System.Threading.Timer.dll"
    },
    {
      "hash": "sha256-gbkL8KvFBaPtvXHY9ybAIq8GKbmntjJ+WujaonT3Pws=",
      "url": "_framework\/System.Threading.dll"
    },
    {
      "hash": "sha256-wLCp1npz0TyKAKE6r0z3TU6IRU09RDYuPFI0T8ww4e0=",
      "url": "_framework\/System.Transactions.Local.dll"
    },
    {
      "hash": "sha256-z9TGdm8XDxY5jN+KtdM3EHrW8ZEJWQFjmeuOz0khuEE=",
      "url": "_framework\/System.Transactions.dll"
    },
    {
      "hash": "sha256-enayKT0ZHazJ0xzfO\/rAEXFX5GMNoMkK362BkzviZj4=",
      "url": "_framework\/System.ValueTuple.dll"
    },
    {
      "hash": "sha256-YLcCK6xyCoDQx2hQD2rr3rx8jC079Eq42fVC20ISjaE=",
      "url": "_framework\/System.Web.HttpUtility.dll"
    },
    {
      "hash": "sha256-lV5tRU+fkR\/dUgSYrFWUySk8hc+AiXzl85GzbEbhN1c=",
      "url": "_framework\/System.Web.dll"
    },
    {
      "hash": "sha256-Vfj\/4ZjSPY0BURuCQYHDXGcv5HUHKLwv1PtPRM6ebrs=",
      "url": "_framework\/System.Windows.dll"
    },
    {
      "hash": "sha256-yxWcq+\/TY1NNdSowOO0mWYp0DPnPR+1SbGtnU9dPXpc=",
      "url": "_framework\/System.Xml.Linq.dll"
    },
    {
      "hash": "sha256-I8JgoZrBrHUDm\/YoMo\/Po4qkjRkiENd0GJxvdJ8WTLU=",
      "url": "_framework\/System.Xml.ReaderWriter.dll"
    },
    {
      "hash": "sha256-ukYOA+GoD5v2KlZVAVAfE\/Z4o3mVb9n8Wa4+gEiTzgI=",
      "url": "_framework\/System.Xml.Serialization.dll"
    },
    {
      "hash": "sha256-YkXugdXEAScEmuIbnEJm3kZg\/kaA9SXfzgVectWPoC0=",
      "url": "_framework\/System.Xml.XDocument.dll"
    },
    {
      "hash": "sha256-ftK9+9\/YY09lvrH5fhZ9sCkz3JDZSXq8SzDlbpiWueA=",
      "url": "_framework\/System.Xml.XPath.XDocument.dll"
    },
    {
      "hash": "sha256-RIboi1fTFJU1uY464JvyW7msqTfJqQ5AeJNeqEfk64E=",
      "url": "_framework\/System.Xml.XPath.dll"
    },
    {
      "hash": "sha256-3v\/4V9Uj09dYDCFRS1KoSDgbVQp6DCJB5T7LgXgjO7I=",
      "url": "_framework\/System.Xml.XmlDocument.dll"
    },
    {
      "hash": "sha256-Tz+IiqaLX1tPYzyzSZAWWmijP73\/+NqQd4qaiEcvosY=",
      "url": "_framework\/System.Xml.XmlSerializer.dll"
    },
    {
      "hash": "sha256-rpq8DHzr+J3ZNKRkX+h+ZFvnjaElX+Qid0O6n88EyD4=",
      "url": "_framework\/System.Xml.dll"
    },
    {
      "hash": "sha256-CyDHXmi+nHwizoYTA\/nl2LdEVXSTCETXqXwxr6pb1os=",
      "url": "_framework\/System.dll"
    },
    {
      "hash": "sha256-2Ao68tI\/bud5mMfa2U184ngHEgYvm8+36AQjT5x9v1M=",
      "url": "_framework\/WindowsBase.dll"
    },
    {
      "hash": "sha256-PLXI3LgpeSBLRubbk0NE5mKLhFkAK+NyTl5+yrfaQIY=",
      "url": "_framework\/mscorlib.dll"
    },
    {
      "hash": "sha256-707sNUyuTw3F88lxBqgjCA0i8CVu3eAoramHmCaboYI=",
      "url": "_framework\/netstandard.dll"
    },
    {
      "hash": "sha256-9l82NJborosg7QYuzSLcEBWjywdoqTLRCBSf3NDNeGk=",
      "url": "_framework\/System.Private.CoreLib.dll"
    },
    {
      "hash": "sha256-PqdZnnWqCyFOvwriATuaugrZb1fSL9sP8Hdaz7VtUE8=",
      "url": "_framework\/dotnet.timezones.blat"
    },
    {
      "hash": "sha256-83rUH9jNpBT8kE0vNpz7ZPKlXIAu4\/GZ8tQkB8jruCE=",
      "url": "_framework\/dotnet.wasm"
    },
    {
      "hash": "sha256-aAYuBDK\/9aSmctFbfPh62gWNg4mgsbu0rMqA08corAA=",
      "url": "_framework\/icudt.dat"
    },
    {
      "hash": "sha256-8+0SUStQbNeoI3hDhcpyKdCWhsndM6zxj6jliMsXeIg=",
      "url": "_framework\/icudt_CJK.dat"
    },
    {
      "hash": "sha256-WqIury5JoQwiCF95WfagtEGBRK2mCvBDAzZEsinsL74=",
      "url": "_framework\/icudt_EFIGS.dat"
    },
    {
      "hash": "sha256-NepbHl0cZW1DE7TQbLOqzgNcIWnfgUq5RKaQL5ijw\/s=",
      "url": "_framework\/icudt_no_CJK.dat"
    },
    {
      "hash": "sha256-KLUslOAWsQu8D2Df41l+hsnNa3YImSMypVd4L9M6h9c=",
      "url": "_framework\/pinvoke.h"
    },
    {
      "hash": "sha256-DkHm9E1AsRANYnTD4h1rnpk9iu2VHxtPFPqyxugfSqA=",
      "url": "_framework\/Emcc.props"
    },
    {
      "hash": "sha256-4BBqWttJ55hUQnVOTlTYbKRZIxqUZc2er0kPIvpvtgA=",
      "url": "_framework\/binding_support.js"
    },
    {
      "hash": "sha256-Ph5zxcWN\/KwWzJubrb4UB+Im1iCKyXM3jmrcd8cxiOw=",
      "url": "_framework\/corebindings.c"
    },
    {
      "hash": "sha256-avU6EbZWJZYGU\/qDjvKBnT0Modr8frl7R2EKmoNBf9Q=",
      "url": "_framework\/dotnet_support.js"
    },
    {
      "hash": "sha256-M3BsLH+QB1LPPsDnaT9geG8loPhgROinsNlYSZfhLPY=",
      "url": "_framework\/driver.c"
    },
    {
      "hash": "sha256-\/KzPD\/xxJ0HT4oEXouD\/HddMn6DczI90WA1yM1nbwx8=",
      "url": "_framework\/emcc-default.rsp"
    },
    {
      "hash": "sha256-diHdRGaVJdt9I9nDCQZZFlvln4XL\/sxSWdLOSfcyE9k=",
      "url": "_framework\/library_mono.js"
    },
    {
      "hash": "sha256-\/oPrwv7NdJlQM2SVHjYqiqyPlSlmI5DPNg7NLmZqfOM=",
      "url": "_framework\/pal_random.js"
    },
    {
      "hash": "sha256-hbkNQPz9HtrSlEf8o3Oo99CP0l+BwwkwVe1\/Ji1b1yQ=",
      "url": "_framework\/pinvoke.c"
    },
    {
      "hash": "sha256-8TwGKS0WfIOAKYoiaSo\/Fxw7DyY44BqcwTGJIZzUcYU=",
      "url": "_framework\/dotnet.6.0.0-preview.6.21352.12.js"
    },
    {
      "hash": "sha256-m4KvVRAvlBsP6IzdmNMmOfKySBMeBD9VLR9cCqZ+StY=",
      "url": "_framework\/BlazingChat.Shared.dll"
    },
    {
      "hash": "sha256-awNOzJiO2XQ5ydGBvyYccso\/GOlyw5ttkKM2DDBkgCA=",
      "url": "_framework\/BlazingChat.Shared.pdb"
    },
    {
      "hash": "sha256-w\/oyiv8m8MxKhx0RAsaYqrX\/anx1ddd2QXwoBXK3auY=",
      "url": "_framework\/BlazingChat.Client.dll"
    },
    {
      "hash": "sha256-kMmxmzaASL2CI64TJgqCDBcleWBWPvaYeakV+HmSgqU=",
      "url": "_framework\/BlazingChat.Client.pdb"
    },
    {
      "hash": "sha256-gb9dN4EKviGf6x9dPnkhY81jBlKvhbI9VrQBZBqYsRs=",
      "url": "_framework\/blazor.webassembly.js"
    },
    {
      "hash": "sha256-zkjXRjkNDxGDaTXVzaPecIsb45a7G0cULCYiOilP7Cc=",
      "url": "_framework\/Microsoft.AspNetCore.Authorization.dll.gz"
    },
    {
      "hash": "sha256-54fLMaE211nwajTZMbQTUPui4k9YaQ\/PIsleGFjVAWA=",
      "url": "_framework\/Microsoft.AspNetCore.Components.dll.gz"
    },
    {
      "hash": "sha256-C5YnbQp4KQBPZRL\/r322pdCs5HtvjukHC5fG\/R0zWc0=",
      "url": "_framework\/Microsoft.AspNetCore.Components.Forms.dll.gz"
    },
    {
      "hash": "sha256-+f\/GudrGJfSZb\/4+eSt1viWqg6\/yrXnswRSpbI51sRg=",
      "url": "_framework\/Microsoft.AspNetCore.Components.Web.dll.gz"
    },
    {
      "hash": "sha256-o8F8LEgiUZyVe8CA8i5drhRnt0xdNz0rfxZETKlJsyQ=",
      "url": "_framework\/Microsoft.AspNetCore.Components.WebAssembly.dll.gz"
    },
    {
      "hash": "sha256-LryvrRRNthZN4a4E2kNKPFCBDf6Sjyyzp5uWx2uSL9M=",
      "url": "_framework\/Microsoft.AspNetCore.Metadata.dll.gz"
    },
    {
      "hash": "sha256-YGfoa3b06UU22zzE2zmSm\/PkQPbhjRAp5grepI4iVgo=",
      "url": "_framework\/Microsoft.Extensions.Configuration.dll.gz"
    },
    {
      "hash": "sha256-mov\/\/x8IW6E+e5gqgEaKfBxA0fGrOeZidg04Lo5qES4=",
      "url": "_framework\/Microsoft.Extensions.Configuration.Abstractions.dll.gz"
    },
    {
      "hash": "sha256-8G+bTLcH31t6e0Iekr21vSL7pvDgfEPW4nqD\/bZjqNQ=",
      "url": "_framework\/Microsoft.Extensions.Configuration.Binder.dll.gz"
    },
    {
      "hash": "sha256-wptnOvdGgvCuHjKC7GbPeXh1L0HzFX5ZMlOMtE7rttQ=",
      "url": "_framework\/Microsoft.Extensions.Configuration.FileExtensions.dll.gz"
    },
    {
      "hash": "sha256-HFgScyZTMVzqC0y0wSzlaRBGeJzxB1nJdleww2qbWk8=",
      "url": "_framework\/Microsoft.Extensions.Configuration.Json.dll.gz"
    },
    {
      "hash": "sha256-UuID9uTunO6yyvoqWaYJC992q3TQj5lhZaxbpGOEFQs=",
      "url": "_framework\/Microsoft.Extensions.DependencyInjection.dll.gz"
    },
    {
      "hash": "sha256-0keZnjg73FQHVtp\/dpr7EtgISAiX+sR3TW12+KFbcrk=",
      "url": "_framework\/Microsoft.Extensions.DependencyInjection.Abstractions.dll.gz"
    },
    {
      "hash": "sha256-B5RV+ukxxBlP\/C27BBABjKuhqIw2RBVuDP\/ymNB8Eao=",
      "url": "_framework\/Microsoft.Extensions.FileProviders.Abstractions.dll.gz"
    },
    {
      "hash": "sha256-rHQSl4nrsmB2\/dKGLyCiZetzF8XxOF9iRdjxysQQ7iY=",
      "url": "_framework\/Microsoft.Extensions.FileProviders.Physical.dll.gz"
    },
    {
      "hash": "sha256-w9OrHq+75O0cDUGxrH6ucysrB9KWeK8L8exATYIdCq0=",
      "url": "_framework\/Microsoft.Extensions.FileSystemGlobbing.dll.gz"
    },
    {
      "hash": "sha256-0U3UmkmHQpI\/1dlFtHs7oXoVKhgIv3LuCmsSQHVOKbE=",
      "url": "_framework\/Microsoft.Extensions.Logging.dll.gz"
    },
    {
      "hash": "sha256-VIHXdNntMKBXHAqKKG\/hYxjOoTgb\/f2MarLFfpTcmi4=",
      "url": "_framework\/Microsoft.Extensions.Logging.Abstractions.dll.gz"
    },
    {
      "hash": "sha256-c3v8oOQgjyjbhaRyYb55lknvluvQOiZ4gmpQwg6jg8A=",
      "url": "_framework\/Microsoft.Extensions.Options.dll.gz"
    },
    {
      "hash": "sha256-dJtU7A\/V5n6W9Phtfco9hRM2Zf51ofDiQKLKYX74rtA=",
      "url": "_framework\/Microsoft.Extensions.Primitives.dll.gz"
    },
    {
      "hash": "sha256-IvoYxwBhW6ei510gXnYrVVPrhZu\/YIijvx0ci2PkBqY=",
      "url": "_framework\/Microsoft.JSInterop.dll.gz"
    },
    {
      "hash": "sha256-8Zqm7toCY7QEw1aEr8zRiakovXf3zTJ6BDelIUb4aBw=",
      "url": "_framework\/Microsoft.JSInterop.WebAssembly.dll.gz"
    },
    {
      "hash": "sha256-PNFk1HQuoNflCySGifvuRh01Jfl4EkIEJk6CxtPKxIE=",
      "url": "_framework\/System.IO.Pipelines.dll.gz"
    },
    {
      "hash": "sha256-rJg03EMnteiqjQobTcVYi8WZeWRPjcIKuxVr+gJP9iA=",
      "url": "_framework\/Microsoft.CSharp.dll.gz"
    },
    {
      "hash": "sha256-yUaB9gexl69lzeDumqE4lX2uWrdRJGCSBcBzu\/aofDE=",
      "url": "_framework\/Microsoft.VisualBasic.Core.dll.gz"
    },
    {
      "hash": "sha256-mOI4OQFfXXwUzPLVOoFL5p4cYi0LdC5QHeoe5pVwMHc=",
      "url": "_framework\/Microsoft.VisualBasic.dll.gz"
    },
    {
      "hash": "sha256-bOuyCMFN34GM0KfDcfH7qG7PAY5E52Z4e4PwI5LNCKk=",
      "url": "_framework\/Microsoft.Win32.Primitives.dll.gz"
    },
    {
      "hash": "sha256-dYcm2ENCjYprflQl\/0E3D+ChSTe1kPB75NwCWYCmUMs=",
      "url": "_framework\/Microsoft.Win32.Registry.dll.gz"
    },
    {
      "hash": "sha256-XhA0KDEeDYXCSqqzHS6J4gr+79tmmnKMiCRLuOEUpq8=",
      "url": "_framework\/System.AppContext.dll.gz"
    },
    {
      "hash": "sha256-M6bOMuBd56DFd+JeBDBzmZVcbeQYRnsHIc0w2j1fQvs=",
      "url": "_framework\/System.Buffers.dll.gz"
    },
    {
      "hash": "sha256-MzskqKHE4u+H+R\/l1doUiYVMKhMYHZIhj40V9ZlrN5A=",
      "url": "_framework\/System.Collections.Concurrent.dll.gz"
    },
    {
      "hash": "sha256-SU52a47KUw4pFyFg4LGe7zpnrPAAXKfMcYm1YwZl5Yw=",
      "url": "_framework\/System.Collections.Immutable.dll.gz"
    },
    {
      "hash": "sha256-dYEqJ0OHidYdmVJufI0K4QIRWz8P3zEJN\/XSkLB9jRo=",
      "url": "_framework\/System.Collections.NonGeneric.dll.gz"
    },
    {
      "hash": "sha256-oQ3BOhbtTu8cYPIQ2e39UXR6Tl3wD5bwnHBGAaqZkmk=",
      "url": "_framework\/System.Collections.Specialized.dll.gz"
    },
    {
      "hash": "sha256-rIYG9Y3z4uhgr5pey+LXqIdzq1t+e5H16o4pt6PH0lo=",
      "url": "_framework\/System.Collections.dll.gz"
    },
    {
      "hash": "sha256-\/4ZrxoyufEKXeRWNorwzBpk9uslEJtqzP+yIyk1aY5Y=",
      "url": "_framework\/System.ComponentModel.Annotations.dll.gz"
    },
    {
      "hash": "sha256-SyecJz3luncAFffQ+dVYGo6941xw7ZXt7FIuGvCbJu8=",
      "url": "_framework\/System.ComponentModel.DataAnnotations.dll.gz"
    },
    {
      "hash": "sha256-\/cnoHjUEMHZ9HHValpdT23wv1nBBORflLHYeKBI12SE=",
      "url": "_framework\/System.ComponentModel.EventBasedAsync.dll.gz"
    },
    {
      "hash": "sha256-ofWhSCjdKNYHDALmZZM+7RAK5Kp20XhbMgXifmx+72Q=",
      "url": "_framework\/System.ComponentModel.Primitives.dll.gz"
    },
    {
      "hash": "sha256-I8JXhuZOHSyg5bNJt0XOWJKTbvtFfGl5mdS9WXLkPaA=",
      "url": "_framework\/System.ComponentModel.TypeConverter.dll.gz"
    },
    {
      "hash": "sha256-MfCLm3dO9zb+HkONmuZI9tiveBPrMKV18sECBbY4Bq0=",
      "url": "_framework\/System.ComponentModel.dll.gz"
    },
    {
      "hash": "sha256-Rc1EM9bdU+RBb7KmiWYbDzGHa8kHs0PaWGyDIFcI48c=",
      "url": "_framework\/System.Configuration.dll.gz"
    },
    {
      "hash": "sha256-xUnVDqRJGooJ\/qbjCicrs6zpaU9dJOoX0oyCd6koNVY=",
      "url": "_framework\/System.Console.dll.gz"
    },
    {
      "hash": "sha256-RvpaM4vKwntU5gp+Fv\/PQaUgSr66aJb0PKQhThZhGno=",
      "url": "_framework\/System.Core.dll.gz"
    },
    {
      "hash": "sha256-Mjzoc7wNaKKK1MmSycEkWVLlF9I6oPDxZB5Y6akAVpE=",
      "url": "_framework\/System.Data.Common.dll.gz"
    },
    {
      "hash": "sha256-NSIxEFkK6\/VOnwXjck+R22YlSqcLBB9kQ39dQw996jU=",
      "url": "_framework\/System.Data.DataSetExtensions.dll.gz"
    },
    {
      "hash": "sha256-29KPlRPb0WypRznx4pLtuzenLv+GQdVTwqayqK5gumg=",
      "url": "_framework\/System.Data.dll.gz"
    },
    {
      "hash": "sha256-7vaiWIwUBg+FJuDP9JpEDgBQ7xHOZWp6IsEuUNDFJ0w=",
      "url": "_framework\/System.Diagnostics.Contracts.dll.gz"
    },
    {
      "hash": "sha256-9+tcDqh\/yllUde5hmIiot688JV4XRs3FrpEq10GzDWs=",
      "url": "_framework\/System.Diagnostics.Debug.dll.gz"
    },
    {
      "hash": "sha256-Pc9hlWA3OI3hxSueMKd4NaoN21WdFRSRp8BcRhjboE8=",
      "url": "_framework\/System.Diagnostics.DiagnosticSource.dll.gz"
    },
    {
      "hash": "sha256-3M8eP24gY44I2UU6K+59aUTpqD7B89waQDhJpFOARk8=",
      "url": "_framework\/System.Diagnostics.FileVersionInfo.dll.gz"
    },
    {
      "hash": "sha256-aikhpOriSQMdvXqBI48EPslXNU0DK6VJ5KoEjMeLe0A=",
      "url": "_framework\/System.Diagnostics.Process.dll.gz"
    },
    {
      "hash": "sha256-3e5JDd1QyJFG+LocT7FrTeIvkQ1BYxeXH81qrLj0rSE=",
      "url": "_framework\/System.Diagnostics.StackTrace.dll.gz"
    },
    {
      "hash": "sha256-niq084AajZCHGQMzg2GBisXNeeJoFaEOgjlYXiioxVc=",
      "url": "_framework\/System.Diagnostics.TextWriterTraceListener.dll.gz"
    },
    {
      "hash": "sha256-S+VQq5nWphaPAwpIcQVVFTGXT+gR+aZGZz5STkCsvkc=",
      "url": "_framework\/System.Diagnostics.Tools.dll.gz"
    },
    {
      "hash": "sha256-DGwIm9HGgKoNhKpcqmBMfgS0Z7Ek9bGxO2opTAI8s2s=",
      "url": "_framework\/System.Diagnostics.TraceSource.dll.gz"
    },
    {
      "hash": "sha256-MmUg7ZdBfqPn\/JEtoHQOXjiX5yA2\/Bc2rKNLOiABQqo=",
      "url": "_framework\/System.Diagnostics.Tracing.dll.gz"
    },
    {
      "hash": "sha256-8RTSlLqTaWdUKgQtVd4nn6Di74jX3RyOUrkxHWfEGUY=",
      "url": "_framework\/System.Drawing.Primitives.dll.gz"
    },
    {
      "hash": "sha256-lsq6BJs+A7SkGQd6k6WgmnwknvzYdfA2VIM1wqJOIu8=",
      "url": "_framework\/System.Drawing.dll.gz"
    },
    {
      "hash": "sha256-4hHBRSTtBUxLZ9wR4hs8yDURA7SolEjHgpImcKJqeZI=",
      "url": "_framework\/System.Dynamic.Runtime.dll.gz"
    },
    {
      "hash": "sha256-W8YpHCiA6tjPl0Wv6yDrjek09l+ES9xiJ5XBwQ0zkXw=",
      "url": "_framework\/System.Formats.Asn1.dll.gz"
    },
    {
      "hash": "sha256-tW\/tUQXzYamc7x8h3oEaS0QuSeGXGmZ1\/4L7hbiR8to=",
      "url": "_framework\/System.Globalization.Calendars.dll.gz"
    },
    {
      "hash": "sha256-vHXT9efNIuvvXKoFgXwLERRuRyYYVcrLaBDsg2JshCA=",
      "url": "_framework\/System.Globalization.Extensions.dll.gz"
    },
    {
      "hash": "sha256-tiyKIefV9aoRtg2sCIVaOofgaJDt8b2wYTOmNX1KlZE=",
      "url": "_framework\/System.Globalization.dll.gz"
    },
    {
      "hash": "sha256-rkOmZpgpDTYibe1JVzfcBaQ0Qx\/AnSPZnYf6Tpclbts=",
      "url": "_framework\/System.IO.Compression.Brotli.dll.gz"
    },
    {
      "hash": "sha256-cOZ3Qmy4z\/x4gJx\/HYCSlzF1I4BN2qgrFsrKfme6pQ0=",
      "url": "_framework\/System.IO.Compression.FileSystem.dll.gz"
    },
    {
      "hash": "sha256-Gn9RVDK7wh7ESgJQLD8e9S1TalRWdCLZXF\/tbx5nivU=",
      "url": "_framework\/System.IO.Compression.ZipFile.dll.gz"
    },
    {
      "hash": "sha256-j40OcEI8UAEik\/O164aXzeK+tdKgkpJBQwUhXHVBzXA=",
      "url": "_framework\/System.IO.Compression.dll.gz"
    },
    {
      "hash": "sha256-OAHKyiVvfGQ9FtnBBWa\/2rqObQEk7Z\/31NE6+YwijkE=",
      "url": "_framework\/System.IO.FileSystem.AccessControl.dll.gz"
    },
    {
      "hash": "sha256-2zTVjNeKD8YwkNRCUatNnxgY34YoWwMA65XJF0TLbXU=",
      "url": "_framework\/System.IO.FileSystem.DriveInfo.dll.gz"
    },
    {
      "hash": "sha256-3OPA8Q2HcNFzGIA6cDEOyIw2UaBncfmKrkserZBAmYc=",
      "url": "_framework\/System.IO.FileSystem.Primitives.dll.gz"
    },
    {
      "hash": "sha256-fg+RykTrnHBggo\/efrLGDdHJ+xy3QTdjD3+HJvUXXXQ=",
      "url": "_framework\/System.IO.FileSystem.Watcher.dll.gz"
    },
    {
      "hash": "sha256-KfCvEWb4xL5q\/omox2lepyi9RIgPE47dQBU0A9bwgKc=",
      "url": "_framework\/System.IO.FileSystem.dll.gz"
    },
    {
      "hash": "sha256-tVVAO+JD\/moizWK7HRK\/T83wdfvxk7HautigGxu6AMI=",
      "url": "_framework\/System.IO.IsolatedStorage.dll.gz"
    },
    {
      "hash": "sha256-hI1vkaBKKbz54L04QqvvdXMaPSCzh8ubLD40wwFEjNM=",
      "url": "_framework\/System.IO.MemoryMappedFiles.dll.gz"
    },
    {
      "hash": "sha256-1cY8n87RqNvM4BOsKxMMylu6qEKXaSgimBKIDq6fJRo=",
      "url": "_framework\/System.IO.Pipes.AccessControl.dll.gz"
    },
    {
      "hash": "sha256-3qp9MjSc\/mWMeBPbS6vNX8VJ6xnFTr2DqUi2C6NYOa8=",
      "url": "_framework\/System.IO.Pipes.dll.gz"
    },
    {
      "hash": "sha256-0C7IWkzo7NuRww94Rb4stc5G1ZoAvzbW9BSN\/Uuo8PI=",
      "url": "_framework\/System.IO.UnmanagedMemoryStream.dll.gz"
    },
    {
      "hash": "sha256-XlN5akEHewZdhy64Q5oW4RUTzwGCgR9c31591dhHqfk=",
      "url": "_framework\/System.IO.dll.gz"
    },
    {
      "hash": "sha256-IifIoHYd8ymunP+adzSNtfeTJKbbsHedCnASeaX57yw=",
      "url": "_framework\/System.Linq.Expressions.dll.gz"
    },
    {
      "hash": "sha256-TVPf2rNVcumoSEbNUswDnDbQPKe7ZihCyx4MCdNJbiE=",
      "url": "_framework\/System.Linq.Parallel.dll.gz"
    },
    {
      "hash": "sha256-725ikRLnbfAK0pNKYd3JcOL5LKQEDRcU66xFOu1pCcY=",
      "url": "_framework\/System.Linq.Queryable.dll.gz"
    },
    {
      "hash": "sha256-UzNS9nkKINWbYRiROuiKuXpfOWLOrb3as5MGKmrGiCU=",
      "url": "_framework\/System.Linq.dll.gz"
    },
    {
      "hash": "sha256-AvRGaQaDnXVngrehWLOK+xRMcHlfgCe\/R2+6sq0af2k=",
      "url": "_framework\/System.Memory.dll.gz"
    },
    {
      "hash": "sha256-m3ADv1syjlkWmYxwJytw62eBlagikM9ZeLljak66HRA=",
      "url": "_framework\/System.Net.Http.Json.dll.gz"
    },
    {
      "hash": "sha256-dPRls7yAbrcAqsVJZBeRCTQmvKEo2eVed\/is4yEiOsw=",
      "url": "_framework\/System.Net.Http.dll.gz"
    },
    {
      "hash": "sha256-ZvKXQd9AkOQr3fY40+oC9hfCunYtmprcOx8aNWB8ku4=",
      "url": "_framework\/System.Net.HttpListener.dll.gz"
    },
    {
      "hash": "sha256-d6qlfGlZNCPwtNKUyZp7uXPzZYeGcM91ag8AENnJ3rQ=",
      "url": "_framework\/System.Net.Mail.dll.gz"
    },
    {
      "hash": "sha256-HW2bTC33Dc6Fk8YP3exLNec7\/MY8ZiUbX4Mk0M5SAgk=",
      "url": "_framework\/System.Net.NameResolution.dll.gz"
    },
    {
      "hash": "sha256-KO+K1oyhu+6Yw6Z8EpXqMJOtOD4yTxpXlmXv3mXqnKM=",
      "url": "_framework\/System.Net.NetworkInformation.dll.gz"
    },
    {
      "hash": "sha256-jg42NvVRoeu0AR7R5pmBdtNvg\/Vt7CPSXBvaoGLtWXY=",
      "url": "_framework\/System.Net.Ping.dll.gz"
    },
    {
      "hash": "sha256-QKC1AU50vRADiequZrGDZYdOLym2RKWrYciW4vORAjg=",
      "url": "_framework\/System.Net.Primitives.dll.gz"
    },
    {
      "hash": "sha256-4Rqy2wzOBmCoC9C8dmriySpVsL8dMX9MZbnZVISJg9o=",
      "url": "_framework\/System.Net.Quic.dll.gz"
    },
    {
      "hash": "sha256-bevzdCAXh60gshK63SupXiXr9P3a1kMh06t0dTv6YAE=",
      "url": "_framework\/System.Net.Requests.dll.gz"
    },
    {
      "hash": "sha256-HvOLLj04VHNZt+rB8tfsNiuS\/xVgDIWnbsV6tXBze7g=",
      "url": "_framework\/System.Net.Security.dll.gz"
    },
    {
      "hash": "sha256-dNP8UEICYaz4RJoFu34GDX+2a0x5cxbAkhU5YsOmoyM=",
      "url": "_framework\/System.Net.ServicePoint.dll.gz"
    },
    {
      "hash": "sha256-xHrMB8FX3H5HCwKyihT5BtZoZnuxgkcYnZWwcgeoaFg=",
      "url": "_framework\/System.Net.Sockets.dll.gz"
    },
    {
      "hash": "sha256-cFdpZkuipCQAkjcFW5xhbYcEd1T7jx7hrlcPR9wwcyQ=",
      "url": "_framework\/System.Net.WebClient.dll.gz"
    },
    {
      "hash": "sha256-Ow5OPj+1sI\/iZWsGNN+G+ENUi\/NCLCjjbeCT4YikAzg=",
      "url": "_framework\/System.Net.WebHeaderCollection.dll.gz"
    },
    {
      "hash": "sha256-\/17EZ27ehaJhfE7fyFjIVUV0cqbWfbHe4E0+KPFwyw4=",
      "url": "_framework\/System.Net.WebProxy.dll.gz"
    },
    {
      "hash": "sha256-cb2hsWcXuqQd+rX5h6hrKKUQT5UbDRUxdIQ\/e0\/+wJo=",
      "url": "_framework\/System.Net.WebSockets.Client.dll.gz"
    },
    {
      "hash": "sha256-xdn0hl89R9daVyeiumHsTHOpqdJQIZjDvF+K8ZPjxS8=",
      "url": "_framework\/System.Net.WebSockets.dll.gz"
    },
    {
      "hash": "sha256-saqgP+76I\/5Sq7LPc1wFSRtrISyrD7U2IdHR7A6U2D4=",
      "url": "_framework\/System.Net.dll.gz"
    },
    {
      "hash": "sha256-BpEzGkOZ+fptbSNG2iY1ZpTXcKH4UmVDqO3HBE+3QYk=",
      "url": "_framework\/System.Numerics.Vectors.dll.gz"
    },
    {
      "hash": "sha256-7MNJUsf5OhTXdBT9cKF16FSRs9tulv2CQxhfCc9Wou0=",
      "url": "_framework\/System.Numerics.dll.gz"
    },
    {
      "hash": "sha256-eRY3EHylCorOd2TH3Oafln8PF44M0zwoCSHqbFtUNa8=",
      "url": "_framework\/System.ObjectModel.dll.gz"
    },
    {
      "hash": "sha256-qsW9dDkHCm3TTkZrlFnZOimevBOzpNJLUDmjSOVkfh4=",
      "url": "_framework\/System.Private.DataContractSerialization.dll.gz"
    },
    {
      "hash": "sha256-BphVu+DgtIDp0BGLFMedRgxY0RUx+GqqEx5CrfP8FIo=",
      "url": "_framework\/System.Private.Runtime.InteropServices.JavaScript.dll.gz"
    },
    {
      "hash": "sha256-oNN6qROk6Xe5DmurV3Z3BCNhwlJYAACXXEeeqiJMPfY=",
      "url": "_framework\/System.Private.Uri.dll.gz"
    },
    {
      "hash": "sha256-6i88x\/CMzKM6FO9WLCbDVpAYOvz3E+HS7ghguMINoUg=",
      "url": "_framework\/System.Private.Xml.Linq.dll.gz"
    },
    {
      "hash": "sha256-4Zlgidxmx2JI22KbJoK9sWTmtzdwwJJo3LlqnswG9JE=",
      "url": "_framework\/System.Private.Xml.dll.gz"
    },
    {
      "hash": "sha256-u4lNg4XEBo7QnKIQDttmKq77awp25IE6D7LBqMSw27M=",
      "url": "_framework\/System.Reflection.DispatchProxy.dll.gz"
    },
    {
      "hash": "sha256-u31zQ86fETGNkDOwzhAGJREKTGTvEfwh8L0G6ONRvlQ=",
      "url": "_framework\/System.Reflection.Emit.ILGeneration.dll.gz"
    },
    {
      "hash": "sha256-MMcQ256v9CdhgD+H\/BY4H0IE1dcJ3YdvJC0mBWt8lTM=",
      "url": "_framework\/System.Reflection.Emit.Lightweight.dll.gz"
    },
    {
      "hash": "sha256-HfavaFQhCbHpl\/ouWWX459EihCAzXYDRM92q806xEbw=",
      "url": "_framework\/System.Reflection.Emit.dll.gz"
    },
    {
      "hash": "sha256-yt5IXF5kNlAU0MlIOdv7vTH7k9vOXxInXZHftUEdHJE=",
      "url": "_framework\/System.Reflection.Extensions.dll.gz"
    },
    {
      "hash": "sha256-AAYwQtWSVpenGPwsmwywDnBsGB2H+pZHEYjFHladgxY=",
      "url": "_framework\/System.Reflection.Metadata.dll.gz"
    },
    {
      "hash": "sha256-cUCI0cPBTJSjfk9cuQVv\/bUGfmRTUIDXrXyeNGDZvTs=",
      "url": "_framework\/System.Reflection.Primitives.dll.gz"
    },
    {
      "hash": "sha256-nbZrKS1uJ+dco9U+VBtuLepHkng6JPPzkAwbpAH3uzc=",
      "url": "_framework\/System.Reflection.TypeExtensions.dll.gz"
    },
    {
      "hash": "sha256-83r8BH2CsAjxKFmRm36HZIm++PtY3FqOtHuzgb+h\/Fc=",
      "url": "_framework\/System.Reflection.dll.gz"
    },
    {
      "hash": "sha256-uOdbHLukbqC+Iz8RXIG4XQ6wZWfQmujsN\/dcHKMcFqQ=",
      "url": "_framework\/System.Resources.Reader.dll.gz"
    },
    {
      "hash": "sha256-uhGA2A01rS7yneoJSQeQj4McGzbexGaHoM0d9JkD0mQ=",
      "url": "_framework\/System.Resources.ResourceManager.dll.gz"
    },
    {
      "hash": "sha256-mI68SPTfbiQ3lwEGMiAcxqaAWmGVNIT0ii4p5z8Us9s=",
      "url": "_framework\/System.Resources.Writer.dll.gz"
    },
    {
      "hash": "sha256-VMrb05Ex44TWE6Co4\/f1C8fvm0bmLW3PEjZE1Kab4H0=",
      "url": "_framework\/System.Runtime.CompilerServices.Unsafe.dll.gz"
    },
    {
      "hash": "sha256-D5L6W6zOyn8+JrXoNyhxy4ps2KTu6jZqOCxfPH1TkCA=",
      "url": "_framework\/System.Runtime.CompilerServices.VisualC.dll.gz"
    },
    {
      "hash": "sha256-6w5+B6tx0GsPE1N\/Jx1YXOYg1VrG4bf2ujNLk73172Y=",
      "url": "_framework\/System.Runtime.Extensions.dll.gz"
    },
    {
      "hash": "sha256-a5ZthfqDWPyZObY0quk1aMKkGs\/aqtRAeNo\/LHbV1qU=",
      "url": "_framework\/System.Runtime.Handles.dll.gz"
    },
    {
      "hash": "sha256-\/OAU0u3tKjBvRcs+R8G1lbmRUOsZ9YAUu7GAUSlczjk=",
      "url": "_framework\/System.Runtime.InteropServices.RuntimeInformation.dll.gz"
    },
    {
      "hash": "sha256-fapizbFgywao\/6lccSiaMPYnip\/gIzlcp5oCVtO\/H2E=",
      "url": "_framework\/System.Runtime.InteropServices.dll.gz"
    },
    {
      "hash": "sha256-SsqkJ2aqFQTvOaiMS4w1IFb0g5k\/s\/d419U7PPvCORE=",
      "url": "_framework\/System.Runtime.Intrinsics.dll.gz"
    },
    {
      "hash": "sha256-VudQfHHIsn1KH1Vs2s7bIcnyOJB1jG8VtP76+FIXlIM=",
      "url": "_framework\/System.Runtime.Loader.dll.gz"
    },
    {
      "hash": "sha256-tTHLXjANSjmHXGakrkax9Dyaj+kTQcJRHJMS+drVU\/8=",
      "url": "_framework\/System.Runtime.Numerics.dll.gz"
    },
    {
      "hash": "sha256-2PdB0uXto5kpBZVuZfMn4w+KMaOTi7DBv\/uSZVLGdh0=",
      "url": "_framework\/System.Runtime.Serialization.Formatters.dll.gz"
    },
    {
      "hash": "sha256-PduThsvWq4RHzyY72aKZnEtuvG9XXwpaqXdke9VO+NE=",
      "url": "_framework\/System.Runtime.Serialization.Json.dll.gz"
    },
    {
      "hash": "sha256-qfG3QQZE1cf0ccg17Ez5q4825XKHcxyKOwkltV2Q5ww=",
      "url": "_framework\/System.Runtime.Serialization.Primitives.dll.gz"
    },
    {
      "hash": "sha256-TxS3sjTO7\/+pLIaN30D6S9K0Ix\/nFTrozxGqDdCiMi0=",
      "url": "_framework\/System.Runtime.Serialization.Xml.dll.gz"
    },
    {
      "hash": "sha256-TdiwL6LcJefhZV08xEzJdmm3ugOSoi8mAwqz\/YqxVgY=",
      "url": "_framework\/System.Runtime.Serialization.dll.gz"
    },
    {
      "hash": "sha256-tTFJK\/\/\/lp411OZLHuXiiBeF+76f\/9djquoqHfmaIK0=",
      "url": "_framework\/System.Runtime.dll.gz"
    },
    {
      "hash": "sha256-Iw5MSsXiQAv7qEDLu6EVY2fnD9moBakKcD8hwANnzlM=",
      "url": "_framework\/System.Security.AccessControl.dll.gz"
    },
    {
      "hash": "sha256-hV2fGHFaVDB6MuamAo4nTzh9HXz+P7FisvKgXDHv4K8=",
      "url": "_framework\/System.Security.Claims.dll.gz"
    },
    {
      "hash": "sha256-B8YFl\/moVyT4NU4DGzN8xoshqEQRlk\/8Rh5TvZtEbns=",
      "url": "_framework\/System.Security.Cryptography.Algorithms.dll.gz"
    },
    {
      "hash": "sha256-eRHDfI9bcA7txOUx\/gTb9rD2W7SjJBSlR9q2B+PCvh0=",
      "url": "_framework\/System.Security.Cryptography.Cng.dll.gz"
    },
    {
      "hash": "sha256-r6XzFeTBLXikVtynBZaLTYgjrAOa\/jk6z3iWye6NiV8=",
      "url": "_framework\/System.Security.Cryptography.Csp.dll.gz"
    },
    {
      "hash": "sha256-nlU2t7rd3Ynt1tZg6JsVmVBC9AsN9Mc9WivAr4uFXMA=",
      "url": "_framework\/System.Security.Cryptography.Encoding.dll.gz"
    },
    {
      "hash": "sha256-cpxVRB0CaTh\/cI2lgOTwO7YNv8p8gMyJwl6ycJv27xU=",
      "url": "_framework\/System.Security.Cryptography.OpenSsl.dll.gz"
    },
    {
      "hash": "sha256-6goGIcr8QpGqvy+JtCwuJe1eE+LE4OKAU+ahijHB6Gk=",
      "url": "_framework\/System.Security.Cryptography.Primitives.dll.gz"
    },
    {
      "hash": "sha256-1n8sva\/H8jLzdecTOpFSAyYNjB4VB3InC0X8W7dso6s=",
      "url": "_framework\/System.Security.Cryptography.X509Certificates.dll.gz"
    },
    {
      "hash": "sha256-3eHE0SVg3uwQF43L2SfyJsZPT3s3aareBwh0HVysx1A=",
      "url": "_framework\/System.Security.Principal.Windows.dll.gz"
    },
    {
      "hash": "sha256-P6TYCWTyj12Xylxw4qrcNdmi8iBjWfK\/DJ9VXbG2st0=",
      "url": "_framework\/System.Security.Principal.dll.gz"
    },
    {
      "hash": "sha256-r3GmgH9W3Ed854XkfuoSLBwspuMNf6julmIE\/\/Atqlo=",
      "url": "_framework\/System.Security.SecureString.dll.gz"
    },
    {
      "hash": "sha256-7+PaXFiYozy7XoxanELz\/crmFJpCmM0769mHzY335jw=",
      "url": "_framework\/System.Security.dll.gz"
    },
    {
      "hash": "sha256-rFJG2xD+BxtDZ4LGCqxoMG01pJwICci2sEWx9CLNmJo=",
      "url": "_framework\/System.ServiceModel.Web.dll.gz"
    },
    {
      "hash": "sha256-e2qY+tZ1FW9tokEye32gmb+JjrlOx0j1ygCjLDyM72U=",
      "url": "_framework\/System.ServiceProcess.dll.gz"
    },
    {
      "hash": "sha256-01cm2VW4Ao3AAmjqbwlJiBG7Byg+XC0mI2GsDSWvnCQ=",
      "url": "_framework\/System.Text.Encoding.CodePages.dll.gz"
    },
    {
      "hash": "sha256-P3wknMt+\/mnBUbSHgEEuywABXNkFDb9GVW1VksVV2IE=",
      "url": "_framework\/System.Text.Encoding.Extensions.dll.gz"
    },
    {
      "hash": "sha256-NZ71j\/VNVDle7PNA6ZKzfmmwflM0QeqXQZFGQ9+vEbM=",
      "url": "_framework\/System.Text.Encoding.dll.gz"
    },
    {
      "hash": "sha256-VFMjXsqM21l\/0MFTZhqHRzDvbwFB4xUUdyQNacrVVKY=",
      "url": "_framework\/System.Text.Encodings.Web.dll.gz"
    },
    {
      "hash": "sha256-oTbGWt97bsIl8O1O4o5KqqkaEEEUHsuYqsboY\/E9nM4=",
      "url": "_framework\/System.Text.Json.dll.gz"
    },
    {
      "hash": "sha256-bNebF7WtRcAy\/7Mpi9PkOoZ4yg5gzToZJyEovsgMe+c=",
      "url": "_framework\/System.Text.RegularExpressions.dll.gz"
    },
    {
      "hash": "sha256-4aVd\/xU0HQC9yyS0PxgjreqcN+lkpxvLyFbO\/9swRdY=",
      "url": "_framework\/System.Threading.Channels.dll.gz"
    },
    {
      "hash": "sha256-0eO5Nj+tQXZa4ND7Gr7zvKW2WipnWFAy8L85vn3k7Tg=",
      "url": "_framework\/System.Threading.Overlapped.dll.gz"
    },
    {
      "hash": "sha256-X5o0WzHr5GqQvljLlc3o1ZAYIMSb9lI+TChZgon1oWw=",
      "url": "_framework\/System.Threading.Tasks.Dataflow.dll.gz"
    },
    {
      "hash": "sha256-ZxmADFUo01EO\/pRLC+NPs5VohCWSxa5WYmJjGPkviiY=",
      "url": "_framework\/System.Threading.Tasks.Extensions.dll.gz"
    },
    {
      "hash": "sha256-50YLWdSHT72KHuqx8pc+c6JDHRdn6Eo4vtNXFVsROCw=",
      "url": "_framework\/System.Threading.Tasks.Parallel.dll.gz"
    },
    {
      "hash": "sha256-PRUYizX8pHuliJjnCPxRyOdYvfcTsy1c60ologMJrao=",
      "url": "_framework\/System.Threading.Tasks.dll.gz"
    },
    {
      "hash": "sha256-cxldVvGxZFfdtEO5N8emkAgktpgcx6x0l7XRhouru3U=",
      "url": "_framework\/System.Threading.Thread.dll.gz"
    },
    {
      "hash": "sha256-tikEsiNQIiIk05rbfKIWOtdbKnzDwJuu7swWpWZU4vs=",
      "url": "_framework\/System.Threading.ThreadPool.dll.gz"
    },
    {
      "hash": "sha256-PZ7afmvwfcmhGRUa2Utzm+SVU9Itc0qE8w3R6znSK00=",
      "url": "_framework\/System.Threading.Timer.dll.gz"
    },
    {
      "hash": "sha256-bywq5i1TMjSGk5qrSHR5huq+2O9K+XbYNTAZYEjvMJ8=",
      "url": "_framework\/System.Threading.dll.gz"
    },
    {
      "hash": "sha256-WJ8f\/hfRo0is9nB\/xnWHJEqoFgbNECAdGt8AGp3m\/FY=",
      "url": "_framework\/System.Transactions.Local.dll.gz"
    },
    {
      "hash": "sha256-AzPZ1gnPRtBM7ce+mYgWW2hmuOHY9k8rgRR3e7DfbsY=",
      "url": "_framework\/System.Transactions.dll.gz"
    },
    {
      "hash": "sha256-sbZ1TG29cDIpPkTL21Q4F6Ki\/SuKzZozultC8ci8VZw=",
      "url": "_framework\/System.ValueTuple.dll.gz"
    },
    {
      "hash": "sha256-n0PWXLvaCiuvYqk\/O6KABDHF2BnYNkYQuWnYizJrL00=",
      "url": "_framework\/System.Web.HttpUtility.dll.gz"
    },
    {
      "hash": "sha256-eigGfaSAFQ0wf0Xzw9gWAr2JJ4FaH22n0hySE3XnZa8=",
      "url": "_framework\/System.Web.dll.gz"
    },
    {
      "hash": "sha256-AYCsRsBvNqvIjFTsPCVujnNLH8vQ13\/S6MEZAWXGDsI=",
      "url": "_framework\/System.Windows.dll.gz"
    },
    {
      "hash": "sha256-+Ge9tVpwsvvV5txUHBlH00i6IANOKZj\/X8S6rHwNiVk=",
      "url": "_framework\/System.Xml.Linq.dll.gz"
    },
    {
      "hash": "sha256-ynZOC4cS4AGi308D\/Nah5wsiovVrxEp+d4Cii8lQoXo=",
      "url": "_framework\/System.Xml.ReaderWriter.dll.gz"
    },
    {
      "hash": "sha256-zs3PbYmFzorcNHXOLYkWG6G3LfgYgpl+s9Ym7HInKhE=",
      "url": "_framework\/System.Xml.Serialization.dll.gz"
    },
    {
      "hash": "sha256-OO3irB4TC570GaU5AHwBSJ6JPt4b1GVoeAWdpVb3otU=",
      "url": "_framework\/System.Xml.XDocument.dll.gz"
    },
    {
      "hash": "sha256-DWgM2CRyRGLiVKPnqUXlgqqYZLlbYA3Q\/BEY3blvf6s=",
      "url": "_framework\/System.Xml.XPath.XDocument.dll.gz"
    },
    {
      "hash": "sha256-myLvtnJYQABb4nm0ROBZl896RNle1LFd3vxqdPLUcjk=",
      "url": "_framework\/System.Xml.XPath.dll.gz"
    },
    {
      "hash": "sha256-b1FJPQ+1aufDdtVm3yV\/Th5n9wMGPkSSzYlv\/55rLlQ=",
      "url": "_framework\/System.Xml.XmlDocument.dll.gz"
    },
    {
      "hash": "sha256-R7amJK2NMoJUMUBvpuufhG5KraGifvU8n3ulTfH1J\/8=",
      "url": "_framework\/System.Xml.XmlSerializer.dll.gz"
    },
    {
      "hash": "sha256-e8Z3UVItYs14doSYHF+cYzHpPCvpg+UBjRhfY1aqkjI=",
      "url": "_framework\/System.Xml.dll.gz"
    },
    {
      "hash": "sha256-6iZfilvfIpQ+DDRPv5Z3l5v3V95I8PkBuoAGUl8Ydvg=",
      "url": "_framework\/System.dll.gz"
    },
    {
      "hash": "sha256-AMYhHOUI1tRsFm10WeKOS0hGNEMonF5XIsn84lixG0E=",
      "url": "_framework\/WindowsBase.dll.gz"
    },
    {
      "hash": "sha256-sU14roA8\/2VuJ2OGZsaMgczw7b\/vi39L+pzXg\/zQV4U=",
      "url": "_framework\/mscorlib.dll.gz"
    },
    {
      "hash": "sha256-VXxpHrvpRoBixGWouM8uDcq5at1eYywY5podUcLhd6c=",
      "url": "_framework\/netstandard.dll.gz"
    },
    {
      "hash": "sha256-QVYUbyGOBOOzpcff4M0XPs9mGedtxgcfDmBFPh0Yadc=",
      "url": "_framework\/System.Private.CoreLib.dll.gz"
    },
    {
      "hash": "sha256-MtTg+EQcYH2H2dp8w1DRUmPadoOZxpbgA50iRHYwF8g=",
      "url": "_framework\/dotnet.timezones.blat.gz"
    },
    {
      "hash": "sha256-L1rHpxNdvBmaWIzEb\/XAIcgwzrJszUtvE34DeHfoQhk=",
      "url": "_framework\/dotnet.wasm.gz"
    },
    {
      "hash": "sha256-q+PWV9HdwDRIx5G7YGjwhxzENuurjW93VUgCAaYvIgc=",
      "url": "_framework\/icudt.dat.gz"
    },
    {
      "hash": "sha256-q8\/VZtHU+KFCe+CCCaHtJNclxagThq1LkrOx4lh8TVY=",
      "url": "_framework\/icudt_CJK.dat.gz"
    },
    {
      "hash": "sha256-AOdLyI4RXFYCLrLGuDZIGjuK5zqfNP\/gGkPZNcZRT+w=",
      "url": "_framework\/icudt_EFIGS.dat.gz"
    },
    {
      "hash": "sha256-bV23+p9FhTn8vTSp\/2F525jyMoURSbmVGGTHsnQnwr4=",
      "url": "_framework\/icudt_no_CJK.dat.gz"
    },
    {
      "hash": "sha256-cZCT15HhrzGjzAZqz+umnvLXHicgtCqlwqbWlAYjquw=",
      "url": "_framework\/pinvoke.h.gz"
    },
    {
      "hash": "sha256-dP4Eyb1Y\/r1OEzaYILMDt6lJBnUFYv31nYINdsXmcaY=",
      "url": "_framework\/Emcc.props.gz"
    },
    {
      "hash": "sha256-441NdupsrfBnFOPbiRUA09mpKG6MwOQN\/H3SuMaM+DM=",
      "url": "_framework\/binding_support.js.gz"
    },
    {
      "hash": "sha256-8sT+KPS1lVYX2PvYTJMLhZ1qlpswBj7MXM7wYISEnHQ=",
      "url": "_framework\/corebindings.c.gz"
    },
    {
      "hash": "sha256-6PjdN3YEXi+PXywcEV9Gp2S50Cpf0mcGL4X3WWJh3JY=",
      "url": "_framework\/dotnet_support.js.gz"
    },
    {
      "hash": "sha256-TOrYnPCt+SB563DEmSs69rpAEMS30UyRIcuo5ysueKk=",
      "url": "_framework\/driver.c.gz"
    },
    {
      "hash": "sha256-k4J82L7IXs2mkXyIVbmyt55rsp1p2ctycczIsKNFTls=",
      "url": "_framework\/emcc-default.rsp.gz"
    },
    {
      "hash": "sha256-c\/6wg9UOOn9mCVjT40mm51aNRQCYeBA6hmdJzypWtvc=",
      "url": "_framework\/library_mono.js.gz"
    },
    {
      "hash": "sha256-a7oCt1LFNSpiTRm4UQTJCYmIQeu1tRcbDXkQgF4s5HE=",
      "url": "_framework\/pal_random.js.gz"
    },
    {
      "hash": "sha256-VAAEedG0QUz3QIfpN9RKqwdtUu9+gFeTwL2p2C1TGTo=",
      "url": "_framework\/pinvoke.c.gz"
    },
    {
      "hash": "sha256-PnYJx5JVzNUDIMTFUsF0\/cG\/PM4i+y7lKOPVFDX0T\/k=",
      "url": "_framework\/dotnet.6.0.0-preview.6.21352.12.js.gz"
    },
    {
      "hash": "sha256-s1DFyxDxsYFWhN2sP43LURzFC4sSfwhpM\/+p5LEcblw=",
      "url": "_framework\/BlazingChat.Shared.dll.gz"
    },
    {
      "hash": "sha256-O2SsTno5nCdgdP0QB1dCOF3qCtgRDLqjE2FNoQoHmfk=",
      "url": "css\/app.css"
    },
    {
      "hash": "sha256-rldnE7wZYJj3Q43t5v8fg1ojKRwyt0Wtfm+224CacZs=",
      "url": "css\/bootstrap\/bootstrap.min.css"
    },
    {
      "hash": "sha256-zs4pVQwgYaVWikAK4IfqOlgzA8aV9PxOw8JEepXWy2k=",
      "url": "css\/bootstrap\/bootstrap.min.css.map"
    },
    {
      "hash": "sha256-jA4J4h\/k76zVxbFKEaWwFKJccmO0voOQ1DbUW+5YNlI=",
      "url": "css\/open-iconic\/FONT-LICENSE"
    },
    {
      "hash": "sha256-BJ\/G+e+y7bQdrYkS2RBTyNfBHpA9IuGaPmf9htub5MQ=",
      "url": "css\/open-iconic\/font\/css\/open-iconic-bootstrap.min.css"
    },
    {
      "hash": "sha256-OK3poGPgzKI2NzNgP07XMbJa3Dz6USoUh\/chSkSvQpc=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.eot"
    },
    {
      "hash": "sha256-sDUtuZAEzWZyv\/U1xl\/9D3ehyU69JE+FvAcu5HQ+\/a0=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.otf"
    },
    {
      "hash": "sha256-+P1oQ5jPzOVJGC52E1oxGXIXxxCyMlqy6A9cNxGYzVk=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.svg"
    },
    {
      "hash": "sha256-p+RP8CV3vRK1YbIkNzq3rPo1jyETPnR07ULb+HVYL8w=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.ttf"
    },
    {
      "hash": "sha256-cZPqVlRJfSNW0KaQ4+UPOXZ\/v\/QzXlejRDwUNdZIofI=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.woff"
    },
    {
      "hash": "sha256-aF5g\/izareSj02F3MPSoTGNbcMBl9nmZKDe04zjU\/ss=",
      "url": "css\/open-iconic\/ICON-LICENSE"
    },
    {
      "hash": "sha256-p\/oxU91iBE+uaDr3kYOyZPuulf4YcPAMNIz6PRA\/tb0=",
      "url": "css\/open-iconic\/README.md"
    },
    {
      "hash": "sha256-Jtxf9L+5ITKRc1gIRl4VbUpGkRNfOBXjYTdhJD4facM=",
      "url": "favicon.ico"
    },
    {
      "hash": "sha256-DbpQaq68ZSb5IoPosBErM1QWBfsbTxpJqhU0REi6wP4=",
      "url": "icon-192.png"
    },
    {
      "hash": "sha256-oEo6d+KqX5fjxTiZk\/w9NB3Mi0+ycS5yLwCKwr4IkbA=",
      "url": "icon-512.png"
    },
    {
      "hash": "sha256-VjMhpvrcABkqcBsYBVcbOS+teajIw2IXyJ2Z8aJbFVE=",
      "url": "index.html"
    },
    {
      "hash": "sha256-uBEGvKJBPN82K2kjAf9Trx5bMd9mXmKK+VphnnTMYAA=",
      "url": "manifest.json"
    },
    {
      "hash": "sha256-m5TFf+vlfyhHg2VW2j+yaeONINoI9ie7t4hjg7fXM0M=",
      "url": "BlazingChat.Client.styles.css"
    },
    {
      "hash": "sha256-Y8sS9ndhMioqyjqvA+wkA2mRtxq7Te4hJJU9XEzuwCA=",
      "url": "BlazingChat.Client.bundle.scp.css"
    }
  ],
  "version": "7zDwfr04"
};
